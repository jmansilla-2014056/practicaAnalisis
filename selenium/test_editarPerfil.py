import random
import string
import time

SELENIUM_HUB = 'http://34.125.41.136:4445/wd/hub'

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
driver = webdriver


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


try:

    print("starting driver")
    driver = webdriver.Remote(
        command_executor=SELENIUM_HUB,
        desired_capabilities=DesiredCapabilities.FIREFOX,
    )

    driver.get('http://analisis2.tk/Login')
    print("se abrio pagina Logil")
    time.sleep(2)

    search_login = driver.find_element_by_id("login")
    search_login.clear()
    search_login.send_keys("inexistente@gmail.com")
    time.sleep(1)
    print("se ingreso correo en el login")

    search_pas = driver.find_element_by_id("password")
    search_pas.clear()
    search_pas.send_keys("1234")
    time.sleep(1)
    print("se ingreso password en el login")

    search_btnLogin = driver.find_element_by_id("ingresar")
    search_btnLogin.click()
    time.sleep(4)
    print("este usuario de prueba no debe acceder")

    
    driver.get('http://analisis2.tk/Login')
    print("se abrio pagina Login")
    time.sleep(6)
    search_login = driver.find_element_by_id("login")

    search_login.clear()
    search_login.send_keys("juarezbarneond@gmail.com")
    time.sleep(1)
    print("se ingreso correo en el login")

    search_pas = driver.find_element_by_id("password")
    search_pas.clear()
    search_pas.send_keys("123")
    time.sleep(1)
    print("se ingreso una password en el login")

    search_btnLogin = driver.find_element_by_id("ingresar")
    search_btnLogin.click()
    time.sleep(3)
    print("se ingreso a la pagina correctamente")
    time.sleep(5)

    print("opening the page")
    driver.get('http://analisis2.tk/Perfil')
    time.sleep(5)
    print("se abrio la pagina Perfil")

    search_btn = driver.find_element_by_id("modificaciones_usuario")
    search_btn.click()
    print("se clickeo modificar_usuario")
    time.sleep(3)

    search_u = driver.find_element_by_id("name_id")
    search_u.clear()
    search_u.send_keys(id_generator())
    time.sleep(2)
    print("se modifco user")

    search_e = driver.find_element_by_id("email_id")
    search_e.clear()
    search_e.send_keys(id_generator() + "@gmail.com")
    time.sleep(2)
    print("se modifico email")

    search_w = driver.find_element_by_id("password_id")
    search_w.clear()
    search_w.send_keys("123")
    time.sleep(2)
    print("se escribio la password")

    search_btn = driver.find_element_by_id("modificar_id")
    search_btn.click()
    time.sleep(2)

    driver.get('http://analisis2.tk/Perfil')
    time.sleep(4)
    print("se abrio la pagina Perfil")

    search_btn = driver.find_element_by_id("modificaciones_usuario")
    search_btn.click()
    time.sleep(4)

    search_u = driver.find_element_by_id("name_id")
    search_u.clear()
    search_u.send_keys(id_generator())
    time.sleep(2)
    print("se modifco user")

    search_e = driver.find_element_by_id("email_id")
    search_e.clear()
    search_e.send_keys("juarezbarneond@gmail.com")
    time.sleep(2)
    print("se modifico email")

    search_w = driver.find_element_by_id("password_id")
    search_w.clear()
    search_w.send_keys("123")
    time.sleep(2)
    print("se escribio la password")

    search_btn = driver.find_element_by_id("modificar_id")
    search_btn.click()
    time.sleep(2)
    print("se modifico el usuario")
    time.sleep(1)
    
    search_btn = driver.find_element_by_id("cerrar")
    search_btn.click()
    print("se cerro sesion del el usuario")
    time.sleep(1)
    


 #CRUD CARPETA
    driver.get('http://analisis2.tk/Login')
    print("CRUD Carpeta")
    time.sleep(2)
    search_login = driver.find_element_by_id("login")
   
    search_login.clear()
    search_login.send_keys("juarezbarneond@gmail.com")
    time.sleep(1)
    
    search_pas = driver.find_element_by_id("password")
    search_pas.clear()
    search_pas.send_keys("123")
    time.sleep(1)
    
    search_btnLogin = driver.find_element_by_id("ingresar")
    search_btnLogin.click()
    time.sleep(4)
    search_carroot = driver.find_element_by_id("divcarpeta13")
    search_carroot.click()
    search_carroot.click()
    search_btnCarpeta = driver.find_element_by_id("NuevaCarpeta")
    search_btnCarpeta.click()
    time.sleep(1)
    search_btnCarpeta = driver.find_element_by_id("recipient-name")
    search_btnCarpeta.clear()
    search_btnCarpeta.send_keys("CarpetaPrueba")
    time.sleep(1)
    search_btncrearcarpeta = driver.find_element_by_id("agregarCarpeta")
    search_btncrearcarpeta.click()
    alert = driver.switch_to_alert()
    alert.accept()
    time.sleep(2)
    search_select = Select(driver.find_element_by_id("opciones"))
    search_select.select_by_index(1)
    time.sleep(1)
    search_btnCarpeta = driver.find_element_by_id("recipient-name")
    search_btnCarpeta.clear()
    search_btnCarpeta.send_keys("CarpetaPruebaEditada")
    time.sleep(1)
    search_btnmodCarpeta = driver.find_element_by_id("botonModificar")
    search_btnmodCarpeta.click()
    alert = driver.switch_to_alert()
    alert.accept()
    time.sleep(3)
    search_select = Select(driver.find_element_by_id("opciones"))
    search_select.select_by_index(3)
    time.sleep(1)
    alert = driver.switch_to_alert()
    alert.accept()
    time.sleep(1)
    alert = driver.switch_to_alert()
    alert.accept()
    time.sleep(1)
#------------------------------------------------------------------
    search_btnCarpeta = driver.find_element_by_id("NuevoArchivo")
    search_btnCarpeta.click()
    time.sleep(1)
    search_btnCarpeta = driver.find_element_by_id("recipient-name")
    search_btnCarpeta.clear()
    search_btnCarpeta.send_keys("ArchivoPrueba")
    time.sleep(1)
    search_file = driver.find_element_by_id("FileArchivo")
    search_file.send_keys("/prueba.txt")
    search_btnCarpeta = driver.find_element_by_id("agregarCarpeta")
    search_btnCarpeta.click()
    alert = driver.switch_to_alert()
    alert.accept()
    search_select = Select(driver.find_element_by_id("opciones"))
    search_select.select_by_index(3)
    time.sleep(1)
    alert = driver.switch_to_alert()
    alert.accept()
    time.sleep(1)
    alert = driver.switch_to_alert()
    alert.accept()
    time.sleep(1)

    time.sleep(10)

except Exception as e:
    print(e)


finally:
    time.sleep(10)
    driver.quit()
    print("finish")
