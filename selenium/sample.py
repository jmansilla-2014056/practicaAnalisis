#imports
import json
import sys
import time
import os
import random
import string
import time
#import datetime
#from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.common.by import By
#from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from random import randrange
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys

SELENIUM_HUB = 'http://34.125.41.136:4445/wd/hub'
driver = webdriver



def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))




def test1():

	try:
	    print("starting prueba registro")
	    driver = webdriver.Remote(
		command_executor=SELENIUM_HUB,
		desired_capabilities=DesiredCapabilities.FIREFOX,
	    )

	    print("opening the page")
	    driver.get('http://analisis2.tk/Registrar')
	    time.sleep(4)
	    print("se abrio la pagina")
	    search_u = driver.find_element_by_id("user")
	    search_u.clear()
	    search_u.send_keys(id_generator())
	    time.sleep(1)
	    print("se escribio user")
	
	    

	    search_e = driver.find_element_by_id("email")
	    search_e.clear()
	    search_e.send_keys(id_generator()+"@gmail.com")
	    time.sleep(1)
	    print("se escribio email")

	    search_p = driver.find_element_by_id("password")
	    search_p.clear()
	    search_p.send_keys("123")
	    time.sleep(1)
	    print("se escribio password")
	    search_w = driver.find_element_by_id("confirmPassword")
	    search_w.clear()
	    search_w.send_keys("123")
	    time.sleep(1)
	    print("se escribio confirmPassword")

	    search_t = driver.find_element_by_id("names")
	    search_t.clear()
	    search_t.send_keys(id_generator())
	    time.sleep(1)
	    print("se escribio names")
	    search_t = driver.find_element_by_id("lastName")
	    search_t.clear()
	    search_t.send_keys(id_generator())
	    time.sleep(1)
	    print("se escribio apellido")
	    search_x = driver.find_element_by_id("birthDay")
	    search_x.send_keys("1999-12-12")
	    time.sleep(1)
	    print("se escribio Fecha")
	    search_i = driver.find_element_by_id("phone")
	    search_i.clear()
	    search_i.send_keys(id_generator(8, '1234567890'))
	    time.sleep(1)
	    print("se escribio phone")

	    search_btn = driver.find_element_by_id("boton")
	    search_btn.click()
	    time.sleep(4)
	    print("se inserto el usuario")
	    
	    print("prueba de registro exitosa")	    
	    time.sleep(10)
	    driver.quit()
	    return(1)
	except:
	    driver.quit()		
	    print("error en prueba de registro")
	    return (0)
def test2():
    try:
        print("starting prueba Login Fail")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX
        )

        driver.get('http://analisis2.tk/Login')
        print("se abrio pagina login")
        time.sleep(6)

        search_login = driver.find_element_by_id("login")
        search_login.clear()
        search_login.send_keys("inexistente@gmail.com")
        time.sleep(1)
        print("se ingreso correo en el login")

        search_pas = driver.find_element_by_id("password")
        search_pas.clear()
        search_pas.send_keys("1234")
        time.sleep(1)
        print("se ingreso password en el login")

        search_btnLogin = driver.find_element_by_id("ingresar")
        search_btnLogin.click()
        time.sleep(1)
        print("este usuario de prueba no debe acceder")
        print("prueba de usuari no existente satisfactoriamente")
        time.sleep(4)
        driver.quit()
        return (1)
    except Exception as e:
        driver.quit()		
        print("error en prueba de usuario no existente")
        return (0)
def test3():
    try:
        print("starting prueba Login ")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX
        )

        driver.get('http://analisis2.tk/Login')
        print("se abrio pagina login")
        time.sleep(6)

        search_login = driver.find_element_by_id("login")
        search_login.clear()
        search_login.send_keys("juarezbarneond@gmail.com")
        time.sleep(1)
        print("se ingreso correo en el login")

        search_pas = driver.find_element_by_id("password")
        search_pas.clear()
        search_pas.send_keys("123")
        time.sleep(1)
        print("se ingreso password en el login")

        search_btnLogin = driver.find_element_by_id("ingresar")
        search_btnLogin.click()
        time.sleep(1)
        


        print("Prueba del login exitosa")      
        time.sleep(4)
        driver.quit()
        return (1)
    except Exception as e:
        driver.quit()		
        print("Prueba del login fallida")
        return (0)
def test4():
    try:
        print("starting prueba carpeta ")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX
        )

        driver.get('http://analisis2.tk/Login')
        print("se abrio pagina login")
        time.sleep(6)

        search_login = driver.find_element_by_id("login")
        search_login.clear()
        search_login.send_keys("juarezbarneond@gmail.com")
        time.sleep(1)
        print("se ingreso correo en el login")

        search_pas = driver.find_element_by_id("password")
        search_pas.clear()
        search_pas.send_keys("123")
        time.sleep(1)
        print("se ingreso password en el login")

        search_btnLogin = driver.find_element_by_id("ingresar")
        search_btnLogin.click()
        time.sleep(3)
        print("Login exitoso")
        #SE DA CLICK EN EL ROOT
        search_carroot = driver.find_element_by_id("divcarpeta14")
        search_carroot.click()
        time.sleep(1)
        search_carroot.click()
        time.sleep(3)
        search_btnCarpeta = driver.find_element_by_id("NuevaCarpeta")
        search_btnCarpeta.click()
        print("click en crear carpeta")
        time.sleep(3)
        search_btnCarpeta = driver.find_element_by_id("recipient-name")
        search_btnCarpeta.clear()
        search_btnCarpeta.send_keys("CarpetaPrueba")
        print("Carpeta Creada con exito")
        time.sleep(3)
        search_btncrearcarpeta = driver.find_element_by_id("agregarCarpeta")
        search_btncrearcarpeta.click()
        time.sleep(2)
        search_select = Select(driver.find_element_by_id("opciones"))
        search_select.select_by_index(3)
        time.sleep(4)
        print("Carpeta elimina con exito")
        
        print("prueba de la carpeta realizada con exito")        
        time.sleep(4)
        driver.quit()
        return (1)
    except Exception as e:
        driver.quit()		
        print("prueba de carpeta fallida")
        return (0)
def test5():
    try:
        print("starting prueba Archivo ")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX
        )

        driver.get('http://analisis2.tk/Login')
        print("se abrio pagina login")
        time.sleep(6)

        search_login = driver.find_element_by_id("login")
        search_login.clear()
        search_login.send_keys("juarezbarneond@gmail.com")
        time.sleep(1)
        print("se ingreso correo en el login")

        search_pas = driver.find_element_by_id("password")
        search_pas.clear()
        search_pas.send_keys("123")
        time.sleep(1)
        print("se ingreso password en el login")

        search_btnLogin = driver.find_element_by_id("ingresar")
        search_btnLogin.click()
        time.sleep(3)
        print("Login exitoso")
        #SE DA CLICK EN EL ROOT
        search_carroot = driver.find_element_by_id("divcarpeta14")
        search_carroot.click()
        time.sleep(1)
        search_carroot.click()
        time.sleep(3)
        print("click en crear archivo")
        search_btnCarpeta = driver.find_element_by_id("NuevoArchivo")
        search_btnCarpeta.click()
        time.sleep(3)
        search_btnCarpeta = driver.find_element_by_id("recipient-name")
        search_btnCarpeta.clear()
        idarchivo =id_generator(5,"12345678")
        search_btnCarpeta.send_keys("ArchivoPrueba"+idarchivo)
        print("Nombre al archivo")
        time.sleep(3)
        search_file = driver.find_element_by_id("FileArchivo")
        search_file.send_keys("/prueba.txt")
        time.sleep(3)
        #search_file.send_keys("/ect/adduser.conf")
        search_btnarchivo = driver.find_element_by_id("agregarCarpeta")
        search_btnarchivo.click()
        time.sleep(5)
        print("Archivo creado con exito")
        #se crea el archivo
        #time.sleep(60)
        search_btnCarpeta1 = driver.find_element_by_class_name("ArchivoPrueba"+idarchivo)
        print("encontrado")
        search_btnCarpeta1.click();
        time.sleep(3)  
        search_select = Select(driver.find_element_by_id("opciones"))
        search_select.select_by_index(3)
        time.sleep(3)
        print("Archivo eliminado con exito")
        print("prueba del archivo realizada con exito")        
        
        time.sleep(4)
        driver.quit()
        return (1)
    except Exception as e:
        driver.quit()		
        print("prueba del archivo fallida ")
        return (0)

def __main__():
    msgError= "Error en las pruebas funcionales."
    if(test1()==0):
        raise Exception(msgError)

    if(test2()==0):
        raise Exception(msgError)
    if(test3()==0):
        raise Exception(msgError)
    if(test4()==0):
        #mande un mensaje de error
        raise Exception(msgError)
    if(test5()==0):
        raise Exception(msgError)
    print("Pruebas Funcionales Exitosas")       
     
__main__();     
